package com.api.Resource;

import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Estabelecimento;
import com.api.Entity.Quartos;
import com.api.Repository.QuartosRepository;

@RestController
public class QuartosResource {
	@Autowired
	private QuartosRepository qr;
	
	//Lista todos os quartos
	@RequestMapping(method = RequestMethod.GET, value = "/api/quartos", produces = "application/json")
	public @ResponseBody List<Quartos> getAllQuartos(){
		return qr.findAll();
	}
	
	//Lista Quarto por ID
	@RequestMapping(method = RequestMethod.GET, value = "api/quartos/{id}", produces = "application/json")
	public Quartos getFindByIdQuartos(@PathVariable Long id) throws ObjectNotFoundException {
		Optional<Quartos> quarto = qr.findById(id);
		return quarto.orElseThrow(() -> new ObjectNotFoundException("Estabelecimento não encontrado! ID: " + id 
				+ ", Tipo: " + Estabelecimento.class.getName(), null));
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/quartos", produces = "application/json")
	public ResponseEntity<Quartos> newQuarto(@RequestBody Quartos quarto){
		qr.save(quarto);
		return new ResponseEntity<Quartos>(quarto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/api/quartos/{id}", produces = "application/json")
	public ResponseEntity<Quartos> deleteQuarto(@PathVariable Long id){
		qr.deleteById(id);
		return new ResponseEntity<Quartos>(HttpStatus.OK);
	}
	
	public ResponseEntity<Quartos> updateQuartos(@RequestBody Quartos quarto,@PathVariable Long id) {
		Optional<Quartos> quar = qr.findById(id);
		
		if(!quar.isPresent())
			return ResponseEntity.notFound().build();
		
		quarto.setCod_Quarto(id);
		qr.save(quarto);
		return ResponseEntity.noContent().build();
	}
}