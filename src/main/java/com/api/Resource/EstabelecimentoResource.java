package com.api.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Estabelecimento;
import com.api.Repository.EstabelecimentoRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@RestController
public class EstabelecimentoResource {
	@Autowired
	private EstabelecimentoRepository er;
	
	// Listar Todos os estabelecimentos
	@RequestMapping(value = "/api/estabelecimentos", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Estabelecimento> getAllEst() {
		return er.findAll();
	}
	
	//Listar estabelecimento por id
	@RequestMapping(value = "api/estabelecimentos/{id}", method = RequestMethod.GET, produces = "application/json")
	public Estabelecimento getFindById(@PathVariable Long id) throws ObjectNotFoundException {
		Optional<Estabelecimento> esta = er.findById(id);
		return esta.orElseThrow(() -> new ObjectNotFoundException("Estabelecimento não encontrado! ID: " + id 
				+ ", Tipo: " + Estabelecimento.class.getName()));
	}

	// Criar novo estabelecimento
	@RequestMapping(method = RequestMethod.POST, value = "/api/estabelecimentos", produces = "application/json")
	public ResponseEntity<Estabelecimento> newEstabelecimento(@RequestBody Estabelecimento estabelecimento) {
		er.save(estabelecimento);
		return new ResponseEntity<Estabelecimento>(estabelecimento, HttpStatus.OK);
	}

	// Deletar um estabelecimento
	@RequestMapping(method = RequestMethod.DELETE, value = "/api/estabelecimentos/{id}", produces = "application/json")
	public ResponseEntity<Estabelecimento> deleteEst(@PathVariable Long id) {
		er.deleteById(id);
		return new ResponseEntity<Estabelecimento>(HttpStatus.OK);
	}

	// Atualizar um estabelecimento já existente
	@RequestMapping(method = RequestMethod.PUT, value = "/api/estabelecimentos/{id}", produces = "application/json")
	public ResponseEntity<Object> updadeEstabelecimento(@RequestBody Estabelecimento esta, @PathVariable Long id){
		Optional<Estabelecimento> estab = er.findById(id);
		
		if(!estab.isPresent()) 
			return ResponseEntity.notFound().build();
		
		esta.setCod_Estabelecimento(id);
		er.save(esta);
		return ResponseEntity.noContent().build();
	}	
}