package com.api.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Comanda implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cod_Comanda;
	
	@OneToOne(mappedBy = "comanda")
	private Reserva reserva;

	public Long getCod_Comanda() {
		return cod_Comanda;
	}

	public void setCod_Comanda(Long cod_Comanda) {
		this.cod_Comanda = cod_Comanda;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}