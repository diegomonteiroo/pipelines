package com.api.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.IntSequenceGenerator;

@Entity
@JsonIdentityInfo(generator=IntSequenceGenerator.class)
public class Reserva implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_Reserva;
	
	@OneToOne(orphanRemoval = true)
	@JsonBackReference(value="reserva-quarto")
	private Quartos quarto;
	
	@ManyToMany(mappedBy="reserva", cascade = CascadeType.ALL)
	@JsonBackReference("servico-reserva")
	private List<Servicos> servico = new ArrayList<Servicos>();
	
	@ManyToOne
	@JsonBackReference(value="reserva-cliente")
	@JoinColumn(name = "cliente", nullable = false)
	private Cliente cliente;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Comanda comanda;
	
	public Long getCod_Reserva() {
		return cod_Reserva;
	}
	
	public void setCod_Reserva(Long cod_Reserva) {
		this.cod_Reserva = cod_Reserva;
	}

	public Quartos getQuarto() {
		return quarto;
	}

	public void setQuarto(Quartos quarto) {
		this.quarto = quarto;
	}

	public List<Servicos> getServico() {
		return servico;
	}

	public void setServico(List<Servicos> servico) {
		this.servico = servico;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}